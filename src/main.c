//=========================================================
// LPC1114 Project
//=========================================================
// File Name : main.c
// Function  : Main Routine
//---------------------------------------------------------
// Rev.01 2010.08.01 Munetomo Maruyama
//---------------------------------------------------------
// Copyright (C) 2010-2011 Munetomo Maruyama
//=========================================================
// ---- License Information -------------------------------
// Anyone can FREELY use this code fully or partially
// under conditions shown below.
// 1. You may use this code only for individual purpose,
//    and educational purpose.
//    Do not use this code for business even if partially.
// 2. You can copy, modify and distribute this code.
// 3. You should remain this header text in your codes
//   including Copyright credit and License Information.
// 4. Your codes should inherit this license information.
//=========================================================
// ---- Patent Notice -------------------------------------
// I have not cared whether this system (hw + sw) causes
// infringement on the patent, copyright, trademark,
// or trade secret rights of others. You have all
// responsibilities for determining if your designs
// and products infringe on the intellectual property
// rights of others, when you use technical information
// included in this system for your business.
//=========================================================
// ---- Disclaimers ---------------------------------------
// The function and reliability of this system are not
// guaranteed. They may cause any damages to loss of
// properties, data, money, profits, life, or business.
// By adopting this system even partially, you assume
// all responsibility for its use.
//=========================================================

#ifdef __USE_CMSIS
#include "LPC11xx.h"
#endif

#include "array_com.h"
#include "color_led.h"
#include "oled.h"
#include "systick.h"
#include "uart.h"

//===============================
// XBEE Terminal
//===============================
//-----------------------
// Main Routine
//-----------------------
int main(void)
{
    uint32_t port;
//    char *pStr;
    uint8_t ch;
    uint32_t data;
    uint32_t posx, posy;

    uint8_t draw_done,  receive_done;
    uint8_t color[4] = {0, 0, 0, 0};

    uint8_t x;
    int8_t y;
    uint8_t cnt;

    //
    // Initialization
    //
    Init_SysTick();
    Init_Array_COM();
    Array_COM_ID_Assignment();
    Init_Color_LED();
    Init_OLED();
    UARTInit(9600);
    //
    posx = 0;
    posy = 7;
    OLED_Clear_Screen(OLED_BLK); // Clear Screen
    OLED_Draw_Char('_', posx, posy, OLED_WHT, OLED_BLK, OLED_FONT_SMALL);

    draw_done = receive_done = 0;
    x = 0;
    y = 127;
    cnt = 0;

    /*
    {
        uint8_t i;
        uint8_t w = 2;
        uint8_t yy = 100;
        for (i=0; i<128; i+=w) {
            OLED_Draw_Dot(i, yy+(w*0), w, 0xffffffff); // white
            OLED_Draw_Dot(i, yy+(w*1), w, 0x0000ffff); // white
            OLED_Draw_Dot(i, yy+(w*2), w, 0x0000f800); // r
            OLED_Draw_Dot(i, yy+(w*3), w, 0x000007e0); // g
            OLED_Draw_Dot(i, yy+(w*4), w, 0x0000001f); // b
        }

    }
    */

    //
    // Main Loop
    //
    while(1)
    {
        //
        // Draw Color LED
        //
//        Draw_Color_LED();
        //
        // If Detect UART Rx Data,
        // send back to Tx, and
        // send Char Tx Packet to all Array COM Port.
        //
        if (UARTReceive_Check())
        {
            ch = UARTReceive_Byte();
            color[cnt] = ch;
            cnt = (cnt+1) % 4;
            if (cnt == 0) {
                // received 1 pixel --> send to OLED
                for (port = 0; port < 4; port++)
                {
                    if (Array_COM_Port_Open(port))
                    {
//                        Array_COM_Tx_Data32(port, *pcolor32);
                        Array_COM_Tx_Multi_Bytes(port, color, 4);
                    }
                }
            }
        }

        //
        // If Detect Rx Packet from Array COM Port,
        // draw Char on OLED.
        //
        if (draw_done == 1) { Draw_Color_LED(); continue; }

        //for (port = 0; port < 4; port++)
        {
            port = E;
            if (Array_COM_Port_Open(port))
            {
                if (Array_COM_Rx_Data32(port, &data, 0))
//                if(Array_COM_Rx_Multi_Bytes(port, ptr, 0))
                {
                    OLED_Draw_Dot(x,   y, 1, (uint32_t)(data));
                    OLED_Draw_Dot(x+1, y, 1, (uint32_t)(data>>16));

                    x += 2;
                    if (x >= 127) {x = 0; y--;}
                    if ((y < 0) && (x >= 127)) draw_done = 1;

                    /*
                    {
                        uint32_t *pc = (uint32_t *)&data;
                        ch = (uint8_t)data;
                        OLED_printf_Position(0, 4);
                        OLED_printf("%03d\n", rcv_count);

                        OLED_Draw_Char(*pc++, posx++, posy, OLED_WHT, OLED_BLK, OLED_FONT_SMALL);
                        OLED_Draw_Char(*pc++, posx++, posy, OLED_WHT, OLED_BLK, OLED_FONT_SMALL);
                        OLED_Draw_Char(*pc++, posx++, posy, OLED_WHT, OLED_BLK, OLED_FONT_SMALL);
                        OLED_Draw_Char(*pc++, posx++, posy, OLED_WHT, OLED_BLK, OLED_FONT_SMALL);
                    }
                    */
                }
            }
        }
    }
    return 0;
}

//=========================================================
// End of Program
//=========================================================
